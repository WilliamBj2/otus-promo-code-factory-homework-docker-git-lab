﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var assembly = typeof(DataContextFactory).GetTypeInfo().Assembly;
            var assemblyName = assembly.GetName().Name;

            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            optionsBuilder
                .UseNpgsql("Host=localhost;Database=promocodefactory;Username=WilliamBj2;Password=TeddyBeer97",
                opt =>
                {
                    opt.MigrationsAssembly(assemblyName);
                });

            return new DataContext(optionsBuilder.Options);
        }
    }
}
